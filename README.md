# docker-lnmp
docker lnmp 多容器部署方案。完全基於docker官方映像檔，遵循最佳實踐，一容器一進程。

## docker 基礎
docker 的基礎用法請參考官方文件

[中文文件參考](https://github.com/yeasy/docker_practice/blob/master/SUMMARY.md)

## docker-compose

docker-compose 是用來管理編排多個容器協作的工具

通過 docker-compose.yml 來編排 nginx、php、mysql 之間的通信和協作

在docker-lnmp 目錄下通過命令 docker-compose up 啟動容器

然後通過 localhost 或者 localhost:8000 就可以訪問 index.php 了

## docker-compose.yml簡單介紹

### Mysql

先從 Mysql 開始，這個 Mysql 映像檔直接來自於官方，沒有任何修改。

```yml
    mysql: ### 容器名稱
      image: mysql ### 官方映像檔 最新版本
      volumes:
        - mysql-data:/var/lib/mysql ### 數據卷，mysql數據就存放在這裏
      ports:
        - "3306:3306" ###端口映射，主機端口:容器對外端口
      environment:
        - MYSQL_ROOT_PASSWORD=123456  ### 設置環境變量，這個變量名是官方映像檔定義的。
    
```
[官方Mysql映像檔構建參考](https://github.com/dockerfile/mysql)

### PHP

PHP 映像檔也來自與官方，但是官方映像檔並沒有提供連接Mysql相關的 pdo_mysql 擴展，這裏做了一點修改，所以不能直接用 image 來依賴官方映像檔，需要單獨寫一個 Dockerfile 來自定義 PHP 映像檔。

```yml
    php-fpm:
      build:
        context: ./php ### 自定義 PHP 映像檔的配置目錄
      volumes:
        - ./www:/var/www/html ### 主機文件與容器文件映射共享，PHP 代碼存這裏
      expose:
        - "9000" ### 容器對外暴露的端口
      depends_on:
        - mysql ### 依賴並鏈接 Mysql 容器，這樣在PHP容器就可以通過 mysql 作為主機名來訪問 Mysql 容器了
```

自定義 PHP 映像檔的配置文件 Dockerfile

```
### 來自官方的PHP映像檔版本為 7.1-fpm.
### 該版本只包含 FPM 不包括 CLI，所以這裡並不能執行 composer
### 如果需要用 PHP-CLI 需要再開一個 CLI 容器，或者安裝同時包含 FPM 和 CLI 的版本
FROM php:7.1-fpm 

### 設置環境變量
ENV TZ=Asia/Taipei

### 執行 bash 命令安裝php所需的擴展
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
    ### 這裏是docker提供的安裝 php 套件的方法，在這裏安裝了pdo_mysql 擴展還有 GD library 等
    && docker-php-ext-install -j$(nproc) iconv mcrypt mysqli pdo_mysql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd
### 複製 php.ini 到 docker 中
COPY ./php.ini /usr/local/etc/php/conf.d/php.ini
```

## Nginx

Nginx 需要配置一個 server，所以也需要一點簡單的設定

```yml
    nginx:
      build:
        context: ./nginx ### 自定義 Nginx 映像檔的配置目錄
      volumes:
          - ./www:/var/www/html 主機文件與容器文件映射共享，PHP 代碼存這裏
      ports:
          - "80:80" ### 端口映射，如果你主機80端口被占用，可以用8000:80
          - "443:443"
      depends_on:
          - php-fpm ### 依賴並連接 PHP 容器，這樣在 Nginx 容器就可以通過 php-fpm 作為主機名來訪問 PHP 容器了
```

自定義 Nginx 映像檔的配置文件 Dockerfile

```yml
FROM nginx:1.11 ### 官方映像檔

ENV TZ=Asia/Taipei ### 環境變量

COPY ./nginx.conf /etc/nginx/conf.d/default.conf ### server配置
```

Nginx server 配置

```
server {

    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    server_name localhost;
    root /var/www/html;
    index index.php index.html index.htm;

    location / {
         try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        
        ### 主要是這裏用 php-fpm:9000來訪問PHP容器
        fastcgi_pass php-fpm:9000;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }

}
```

## volumes 數據卷

數據卷獨立與容器之外專門存放數據的

```
### 這裏定義了 mysql 所用到的數據卷
volumes:
    mysql-data:
```

## php 測試程式

```php
<?php
// 建立連接
try{
	//這裏的mysql:host=mysql,後面這個mysql就是我們的mysql容器
	//用戶名 root 是默認的
	//密碼 123456 就是我們在mysql容器設置的環境變量
	$dbh = new PDO("mysql:host=mysql;dbname=mysql", "root", 123456);
	$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$dbh->exec("SET CHARACTER SET utf8");
	$dbh=null; //斷開連接	
}catch(PDOException $e){
	print"Error!:".$e->getMessage()."<br/>";
	die();
}
// 錯誤檢查S
// 輸出成功連接
echo "<h1>成功通過 PDO 連接 MySQL 服務器</h1>" . PHP_EOL;

phpinfo();

?>
```

## Laravel 的建立

當 docker 都 run 起來後，接下來要先進入 workspace 的 www 資料夾（與 docker-lnmp 同一層）

```
cd www

git clone git@gitlab.com:capernaum.nctu.me/capernaum.git ./
```

然後執行容器
```
docker-compose exec php-cli bash

cd /var/www/html/

composer install

cp .env.example .env

php artisan key:generate
```

瀏覽器訪問 http://localhost:8000/ 出現歡迎頁面。
